FROM python:3.7-alpine

MAINTAINER yangyuhang

WORKDIR /app

COPY . /app

RUN sed -i 's/dl-cdn.alpinelinux.org/mirror.tuna.tsinghua.edu.cn/g' /etc/apk/repositories &&\
    apk add --no-cache --virtual=build-dependencies \
         g++ \
        build-base libffi-dev python3-dev \
        libffi openssl ca-certificates gcc musl-dev libxslt-dev libressl-dev \
        jpeg-dev zlib-dev freetype-dev lcms2-dev openjpeg-dev tiff-dev tk-dev tcl-dev \
        linux-headers pcre-dev  && \
    apk add --no-cache tzdata && \
    cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    python -m pip install --upgrade pip -i https://mirrors.aliyun.com/pypi/simple/ && \
    pip install --no-cache-dir -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple/ && \
    apk del g++ && \
    apk del --purge \
    build-dependencies && \
    rm -rf /root/.cache \
    /tmp/*

# 项目介绍
- 监听钉钉开放平台员工入职/离职事件的接口，自动更新ldap中的组织架构

# api架构
- Flask框架

# 启动api
- python app.py

# 主要接口
- /add_user: 新员工入职，加入ldap服务
- /remove_user: 老员工离职，从ldap中删除

# 接口请求方式
- 只接受POST请求

# 接口数据
- data = {
    'name': '张三',
    'userid': '123432141',
    'position': '后端',
    'department': 323361303
}
- 该数据一般从钉钉开放平台中监听员工离职与入职事件，钉钉回调获取数据.
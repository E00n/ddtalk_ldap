# ldap服务器配置
LDAP_CONFIG = {
    'host': '172.17.0.1',
    'port': 389,
    'base_dn': 'dc=yang,dc=com',
    'user': 'admin',
    'password': 'yang123'
}

# 钉钉配置
AgentID = 'xxxx'
AppKey = 'dingxxxx'
AppSecret = 'xxxxxxxxxxx'

# 日志配置
logger_dict = {
    'version': 1, # 该配置写法固定
    'formatters': { # 设置输出格式
        'default': {'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',}
    },
    # 设置处理器
    'handlers': {
        'wsgi': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
            'formatter': 'default',
            'level': 'DEBUG'
                }},
    # 设置root日志对象配置
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    },
    # 设置其他日志对象配置
    'loggers': {
        'test':
            {'level': 'DEBUG',
             'handlers':['wsgi'],
             'propagate':0}
    }
}

# jumpserver数据库配置
DB_HOST = '192.168.25.130'
DB_PORT = 3306
DB_USER = 'jms'
DB_PASSWORD = 'jms@ssss'
DB_NAME = 'jumpserver'

# 机器人地址
WEBHOOK = 'https://oapi.dingtalk.com/robot/send?access_token' \
          '='

# 通知人号码
MOBILE_NUMBER = ""